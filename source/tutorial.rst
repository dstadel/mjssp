Tutorial
========

Afin que ce site reste vivant, vous trouverez ci-dessous des informations permettant à toute personne ayant des notions de base en site web de comprendre comment le site est structuré, et pourquoi certains choix ont été faits.
Toutefois, dans un but de sécurité du site, certaines informations n'y figurent pas, notamment les mots de passe.

Comment le site est-il construit ?
----------------------------------
Nous avons choisi d'appuyer le site sur les technologies suitantes :

- Wordpress_ : Editeur de site web très populaire, open source, utilisé dans environ 60% des sites web existants en 2017. 
- Plugins **sécurité** Wordpress :

  - Akismet_ Anti-Spam : permet d'éviter que des utilisateurs extérieurs commentent de manière inappropriée.
  - UpdraftPlus_ : Sauvegarde et restauration. Permet une sauvegarde des bases de données chaque nuit et de déposer ces fichiers sur un serveur externe, permettant ainsi un reconstruction complète en cas de nécessité.
  - WordFence_ : Sécurité, antivirus, parre-feu, mises à jour automatiques.

- Plugins **fonctionnels** Wordpress:

  - `Translator Revolution`_ : Traduction automatique des pages
  - Manquent : Gestion des membres

- Thèmes: **Mise en forme** des pages et des articles

  - Divi_ : mise en forme de pages par coller-déposé

.. _Wordpress: https://fr.wikipedia.org/wiki/WordPress
.. _Akismet: https://akismet.com/
.. _UpdraftPlus: https://updraftplus.com/
.. _WordFence: http://www.wordfence.com/
.. _`Translator Revolution`: http://www.surstudio.net/translator-revolution-dropdown/
.. _Divi: https://www.elegantthemes.com/gallery/divi/

Quels sont les automatismes ?
-----------------------------
Voici les automatismes

Comment créer de nouveaux articles ?
------------------------------------
Voici comment créer un nouvel article...