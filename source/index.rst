.. mjssp-wp documentation master file, created by
   sphinx-quickstart on Tue Mar 13 10:14:21 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur l'aide de maintenance du site du MJSSP !
====================================

Ceci est la documentation officielle pour comprendre 
* la manière dont le site du mjssp a été construit
* les automatismes du site
* comment insérer de nouveaux articles

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   intro
   tutorial



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
